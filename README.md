<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae V0.2.85 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev_tpl_namespace_root V0.3.6 -->
# db_core 0.3.13

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_db_core/develop?logo=python)](
    https://gitlab.com/ae-group/ae_db_core)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_db_core/release0.2.13?logo=python)](
    https://gitlab.com/ae-group/ae_db_core/-/tree/release0.2.13)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_db_core)](
    https://pypi.org/project/ae-db-core/#history)

>ae namespace module portion db_core: database connection and data manipulation base classes.

[![Coverage](https://ae-group.gitlab.io/ae_db_core/coverage.svg)](
    https://ae-group.gitlab.io/ae_db_core/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_db_core/mypy.svg)](
    https://ae-group.gitlab.io/ae_db_core/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_db_core/pylint.svg)](
    https://ae-group.gitlab.io/ae_db_core/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_db_core)](
    https://gitlab.com/ae-group/ae_db_core/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_db_core)](
    https://gitlab.com/ae-group/ae_db_core/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_db_core)](
    https://gitlab.com/ae-group/ae_db_core/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_db_core)](
    https://pypi.org/project/ae-db-core/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_db_core)](
    https://gitlab.com/ae-group/ae_db_core/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_db_core)](
    https://libraries.io/pypi/ae-db-core)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_db_core)](
    https://pypi.org/project/ae-db-core/#files)


## installation


execute the following command to install the
ae.db_core module
in the currently active virtual environment:
 
```shell script
pip install ae-db-core
```

if you want to contribute to this portion then first fork
[the ae_db_core repository at GitLab](
https://gitlab.com/ae-group/ae_db_core "ae.db_core code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_db_core):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_db_core/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.db_core.html#module-ae.db_core
"ae_db_core documentation").
